import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/duck_fly_behavior.dart';
import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_quackbehavior.dart';
import 'package:dart_design_patterns/strategy_pattern/strategy_pattern.dart';
import 'package:test/test.dart';

void main() {
  group('RedheadDuck', () {
    test('can swim', () {
      // setup
      final duck = RedheadDuck();

      // execute
      final result = duck.swim();

      // match
      expect(result, SWIM_STRING);
    });

    test('can quack', () {
      // setup
      final duck = RedheadDuck();

      // execute
      final result = duck.quack();

      // match
      expect(result, QUACK_STRING);
    });

    test('can display', () {
    // setup
      final duck = RedheadDuck();

      // execute
      final result = duck.display();

      // match
      expect(result, DISPLAY_DUCK_STRING);
      
    });

     test('can fly', () {
    // setup
      final duck = RedheadDuck();

      // execute
      final result = duck.fly();

      // match
      expect(result, '$RED_HEAD_DUCK_STRING\n$FLY_STRING');
      
    });
  });
}
