import 'package:dart_design_patterns/strategy_pattern/quck_behavior/squeak_quack_behavior.dart';
import 'package:dart_design_patterns/strategy_pattern/strategy_pattern.dart';
import 'package:test/test.dart';

void main() {
  group('RubberDuck', () {
    test('can swim', () {
      // setup
      final duck = RubberDuck();

      // execute
      final result = duck.swim();

      // match
      expect(result, SWIM_STRING);
    });

    test('can quack', () {
      // setup
      final duck = RubberDuck();

      // execute
      final result = duck.quack();

      // match
      expect(result, SQUEAK_STRING);
    });

    test('can display', () {
      // setup
      final duck = RubberDuck();

      // execute
      final result = duck.display();

      // match
      expect(result, DISPLAY_DUCK_STRING);
    });

    
  });
}
