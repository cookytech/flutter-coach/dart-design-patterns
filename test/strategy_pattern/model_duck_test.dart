import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/fly_rocket_powered.dart';
import 'package:dart_design_patterns/strategy_pattern/model_duck.dart';
import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_quackbehavior.dart';
import 'package:dart_design_patterns/strategy_pattern/strategy_pattern.dart';
import 'package:test/test.dart';

void main() {
  group('ModelDuck', () {
    test('can swim', () {
      // setup
      final duck = ModelDuck();

      // execute
      final result = duck.swim();

      // match
      expect(result, SWIM_STRING);
    });

    test('can quack', () {
      // setup
      final duck = ModelDuck();

      // execute
      final result = duck.quack();

      // match
      expect(result, QUACK_STRING);
    });

    test('can display', () {
      // setup
      final duck = ModelDuck();

      // execute
      final result = duck.display();

      // match
      expect(result, DISPLAY_DUCK_STRING);
    });

    test('can fly', () {
      // setup
      final duck = ModelDuck();

      // execute
      final result = duck.fly();

      // match
      expect(result, FLY_WITH_ROCKET_STRING);
    });
  });
}
