# Dart Design Patterns

An implementation of GOF design patterns using dartlang features. Recommended practices, and learned lessons.

## Design Patterns Introduction

As object oriented programming became popular, people started noticing that they were solving the same problems over and over again. They had noticed that there were `best solutions` to such problems.

A group of four influential developers 

* Eric Gamma
* Richard Helm
* Ralph Johnson
* John Vlissides

Doccumented the best methods to solve common occuring problems and quoted the term `'Design Patterns'`. They published their findings in this book:

`Design Patterns` (Affiliate Link)

[![Design Patterns](https://images-na.ssl-images-amazon.com/images/I/51%2B4NnLvvYL._SX363_BO1,204,203,200_.jpg)](https://amzn.to/2s0U8eF)

Soon `Design Patterns` became an Industry norm, and the common language that efficient developers use to communicate solutions in.

Later, `O'REILLY`'s Head First series got a whole book on Design Paterns. Head First series has been beautiuly forged to explain the concepts in a way that is 'brain-friendly'.

`Head First Design Patterns` (Affiliate Link)

[![Head First Design Patterns](https://images-na.ssl-images-amazon.com/images/I/51PqAvdWZCL._SX425_BO1,204,203,200_.jpg)](https://amzn.to/2GGsF6w)

## Design Patterns and Dart

Design patterns are mainly relevant to `object-oriented programming`. They allow us to re-use solutions to common problems and not make the mistaes that people made before us.
They help users in writing functional and maitainable code.

`Dart` is a purely object oriented programming language and has some amazing features that will support implemntation of design patterns. Although the books are written for Java, 

* we will implement the same examples as in the book, with dart.
* we will leverage dart features to make the implementation of these patterns as simple and easy as possible.
* we will make sure we follow dartlang's [`Effective Dart`](https://www.dartlang.org/guides/language/effective-dart) guidelines while we're at it.

