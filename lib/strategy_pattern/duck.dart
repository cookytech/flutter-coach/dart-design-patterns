const SWIM_STRING = 'SWIM SWIM';
const DISPLAY_DUCK_STRING = 'I am a DUCK';

abstract class Duck {

  String swim() {
    return SWIM_STRING;
  }

  String fly();

  String quack();

  String display() {
    return DISPLAY_DUCK_STRING;
  }
}

class QuckBehavior {}
