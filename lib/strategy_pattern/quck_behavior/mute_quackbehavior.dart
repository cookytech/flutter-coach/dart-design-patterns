import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_behavior.dart';

const MUTE_STRING = '...';

mixin MuteQuackBehavior implements QuackBehavior {
  @override
  String quack() => MUTE_STRING;
}
