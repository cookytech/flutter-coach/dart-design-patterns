import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_behavior.dart';

const QUACK_STRING = 'quack quack';

mixin QuackQuackBehavior implements QuackBehavior {
  @override
  String quack() => QUACK_STRING;
}
