import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_behavior.dart';

const SQUEAK_STRING = 'squeak squeak';

mixin SqueakQuackBehavior implements QuackBehavior {
  @override
  String quack() => SQUEAK_STRING;
}
