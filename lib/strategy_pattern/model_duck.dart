import 'package:dart_design_patterns/strategy_pattern/duck.dart';
import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/fly_rocket_powered.dart';
import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_quackbehavior.dart';

class ModelDuck extends Duck with FlyRocketPowered, QuackQuackBehavior{}
