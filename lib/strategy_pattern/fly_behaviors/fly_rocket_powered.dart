import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/fly_behavior.dart';

const FLY_WITH_ROCKET_STRING = 'I am flying with a rocket';

mixin FlyRocketPowered implements FlyBehavior {
  @override
  String fly() => FLY_WITH_ROCKET_STRING;
}
