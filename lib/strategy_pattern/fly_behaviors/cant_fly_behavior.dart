import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/fly_behavior.dart';

const CANT_FLY_STRING = 'I CANT  FLY';

mixin CantFlyBehavior implements FlyBehavior {
  @override
  String fly() => CANT_FLY_STRING;
}
