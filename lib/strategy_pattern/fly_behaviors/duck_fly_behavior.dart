import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/fly_behavior.dart';

const FLY_STRING = 'I AM FLYING FLAP FLAP';

mixin DuckFlyBehavior implements FlyBehavior {
  @override
  String fly() => FLY_STRING;
}
