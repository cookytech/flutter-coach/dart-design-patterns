import 'package:dart_design_patterns/strategy_pattern/duck.dart';
import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/duck_fly_behavior.dart';
import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_quackbehavior.dart';

const RED_HEAD_DUCK_STRING = 'I am a red head duck';

class RedheadDuck extends Duck with QuackQuackBehavior, DuckFlyBehavior {
  @override
  String fly() => '$RED_HEAD_DUCK_STRING\n${super.fly()}';
}
