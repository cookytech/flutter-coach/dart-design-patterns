import 'package:dart_design_patterns/strategy_pattern/duck.dart';
import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/duck_fly_behavior.dart';
import 'package:dart_design_patterns/strategy_pattern/quck_behavior/quack_quackbehavior.dart';

class MallardDuck extends Duck with QuackQuackBehavior, DuckFlyBehavior{}
