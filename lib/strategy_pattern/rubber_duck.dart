import 'package:dart_design_patterns/strategy_pattern/duck.dart';
import 'package:dart_design_patterns/strategy_pattern/fly_behaviors/cant_fly_behavior.dart';
import 'package:dart_design_patterns/strategy_pattern/quck_behavior/squeak_quack_behavior.dart';

const CAN_NOT_FLY_STRING = 'rubber ducks can not fly';

class RubberDuck extends Duck with SqueakQuackBehavior, CantFlyBehavior {}
